import java.util.Scanner;

public class Gravitacija {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        double height = input.nextDouble();
        izpisi(height, gravAccel(height));
    }
    
    //Funkcija za izpis nadmorske visine in gravitacijskega pospeska
    public static void izpisi(double h, double g) {
        System.out.println("Nadmorska višina: " + h +"m\nGravitacijski pospešek: " + g + "m/s^2");
    }
    
    //Funkcija za izracun gravitacijskega pospeska za podano nadmorsko visino h (v metrih)
    public static double gravAccel(double h) {
        return 0.00000000006674 * 5972000000000000000000000.0 / ((6371000.0 + h) * (6371000.0 + h));
    }
}